#include "header.h"

pthread_t worksite[NUM_THREADS]; /* tablica stanowisk - watkow */
pthread_mutex_t mutex_payment; /* mutex dla uaktualniania total_sum */
pthread_mutex_t mutex_car_id; /* mutex dla pobierania car_id, repair_cost i uaktualniania counter */

extern int timeOfSimulationInSec;

/* DEKLARACJE METOD */
void * doWorkStation(void *); /* naprawa samochodu na stanowisku */

void generateWorkstations(void) {
	/* INICJALIZACJA MUTEXOW */
	pthread_mutex_init(&mutex_payment, NULL);
	pthread_mutex_init(&mutex_car_id, NULL);

	/* TWORZENIE WATKOW TYPU JOINABLE */
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	long i;
	for (i = 0; i < NUM_THREADS; i++) {
		pthread_create(&worksite[i], &attr, doWorkStation, (void *) i);
	}
	pthread_attr_destroy(&attr);
}

void joinAndTerminateWorkstationsThreads(void) {
	void *status;
	int i;
	for (i = 0; i < NUM_THREADS; i++) {
		pthread_join(worksite[i], &status);
		printf("\nStanowisko nr: %i zakończyło pracę.\n", i);
	}

	/* Wyswietlanie wyniku, czyszczenie zasobow */
	printf("\nZamknięcie warsztatu. Podsumowanie dotychczasowej działalności. "
			"Czas działania: %i sekund.\n"
			"Koszt wszystkich napraw w warsztacie to: %2.2f zł, "
			"liczba naprawionych aut: %d\n\n", timeOfSimulationInSec, total.total_sum,
			total.num_of_repaired_cars);

	pthread_mutex_destroy(&mutex_payment);
	pthread_mutex_destroy(&mutex_car_id);

	/* Czekamy az wszystkie watki zakoncza dzialanie */
	pthread_exit((void*) 0);
}
