#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/queue.h>

#include <sys/types.h>
#include <string.h>

#include  <sys/ipc.h>
#include  <sys/shm.h>

#include <semaphore.h>
#include <fcntl.h>

#include <signal.h>
#include <sys/wait.h>

/* Informacje o zleceniu, trzymane na liscie (TAILQ) */
typedef struct order_data {
    char car_id;
    int damage_degree; //stopien uszkodzenia auta
	double repair_cost;
	TAILQ_ENTRY(order_data) entries;
 } order_data;

 /* Informacje o całkowitym przychodzie i ilosci naprawionych aut */
 typedef struct total_data {
	 double total_sum;
	 int num_of_repaired_cars;
 } total_data;

TAILQ_HEAD(, order_data) order_data_head; /* inicjujemy liste */
struct order_data *item;
struct total_data total;

#define NUM_THREADS 4 /* ilosc stanowisk (watki) */
