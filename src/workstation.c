#include "header.h"

extern pthread_mutex_t mutex_payment;
extern pthread_mutex_t mutex_car_id;

order_data * getJobFromQueue() {
	struct order_data *temp = NULL;
	if (!TAILQ_EMPTY(&order_data_head)) {
		temp = malloc(sizeof(*temp));
		temp = TAILQ_FIRST(&order_data_head);
		TAILQ_REMOVE(&order_data_head, temp, entries);
		//	free(temp);
	}
	return temp;
}

void updateTotalValue(double cost) {
	pthread_mutex_lock(&mutex_payment);
	total.total_sum += cost;
	total.num_of_repaired_cars++;
	pthread_mutex_unlock(&mutex_payment);
}

long getRepairTime(int damageDegree) {
	long repair_time = 0;
	switch (damageDegree) {
	case 1:
		repair_time = 500000;
		break;
	case 2:
		repair_time = 700000;
		break;
	case 3:
		repair_time = 1000000;
		break;
	case 4:
		repair_time = 2000000;
		break;
	case 5:
		repair_time = 3000000;
		break;
	}
	return repair_time;
}

void * doWorkStation(void * threadid) {
	pthread_mutex_lock(&mutex_car_id);
	struct order_data * job = malloc(sizeof(*job));
	job = getJobFromQueue();
	pthread_mutex_unlock(&mutex_car_id);
	long tid = (long) threadid;
	if (job != NULL) {
		long repair_time = getRepairTime(job->damage_degree);
		usleep(repair_time);
		printf("\nNaprawa wykonywana przez stanowisko nr: %ld,"
				" auto nr: %c, stopień uszkodzenia: %d, koszt: %2.2f\n", tid,
				job->car_id, job->damage_degree, job->repair_cost);

		updateTotalValue(job->repair_cost);

		printf("Aktualizacja danych warsztatu, przychód: %2.2f zł, "
				"liczba naprawionych aut: %d\n", total.total_sum,
				total.num_of_repaired_cars);

		free(job);
	}
	sleep((rand()%3)+0.5);
	while (!TAILQ_EMPTY(&order_data_head)) {
		doWorkStation((void *) tid);
	}
	pthread_exit(NULL);
}
