#include "header.h"

#define READ  0
#define WRITE 1

int timeOfSimulationInSec = 10;
int workOn = 1;
int signalFromManager = 0;
int breakFlag = 0;
int damages[] = { 1, 2, 3, 4, 5 }; /* stopnie uszkodzenia, im wiekszy tym dluzej naprawa trwa i jest drozej */
char cars[] = { 'F', 'M', 'P', 'K', 'L', 'B', 'Z', 'V', '\0'};

/* DEKLARACJE METOD */
void addJobToList(int car_id_index, double repair_cost, int damage_degree); /* generuje zlecenia dla stanowisk */
void generateWorkstations(void); /* generuje stanowiska warsztatu (watki) */
void initTotalData(void); /* inicjalizacja struktury total_data */
void joinAndTerminateWorkstationsThreads(void); /* czekanie az wszystkie watki zakoncza prace */
void initRandomNumberGenerator(void); /* Inicjalizacja losowego generatora liczb - funkcji rand() */
void checkTheTime(time_t end); /* Sprawdza czy nie uplynal czas dzialania symulatora */
int forkedFailed(pid_t pid);
void signalWhenManagerSendOrder();
void signalTheManagerMustEnd();
void serviceWork(int * shared_data);

int main(int argc, char * argv[]) {
	TAILQ_INIT(&order_data_head); /* Inicjalizacja kolejki na której umieszczane sa zlecenia */
	time_t end_time = time(NULL) + timeOfSimulationInSec;

	pid_t pid;
	int pipe_fd[2];
	char readbuffer[1024];
	pipe(pipe_fd);
	pid = fork();

	if (forkedFailed(pid)) {
		return 1;
	}

	initRandomNumberGenerator();

	if (pid == 0) {
		/* PROCES KLIENT - GENEROWANIE ZLECEN NAPRAW I PRZEKAZYWANIE
		 * DO PROCESU MANAGERA PRZY POMOCY POTOKU */
		close(pipe_fd[READ]); /* Zamknij wejsciowa strone potoku (pipe) */
		while (workOn) {
			char message[] = "Car id:";
			char car_id = cars[rand() % 8];
			int damage = (rand() % 4) + 1;
			sprintf(message, "%s %c, damage degree: %i\n", message, car_id, damage);
			/* Wyslij message przez wyjsciowa strone potoku */
			write(pipe_fd[WRITE], message, (strlen(message)));
			/* Odstep zanim zostanie wygenerowane kolejne zlecenie */
			long wait_time = (rand() % 1000000) + 10000;
			usleep(wait_time);
			checkTheTime(end_time);
		}
		exit(0);

	} else {
		/* PROCES RODZIC -> TWORZYMY DWA KOLEJNE PROCESY: MANAGER I WARSZTAT */
		int shmid;
		int *manager_shm_data, *service_shm_data;
		shmid = shmget(IPC_PRIVATE, 2 * sizeof(int), 0777 | IPC_CREAT); /* We request an array of two integers */

		pid_t pid2 = fork();
		if (forkedFailed(pid2)) {
			return 1;
		}

		if (pid2 == 0) {
			/* PROCES WARSZTAT, ODBIOR ZLECEN OD MANAGERA - ODCZYT PAMIECI DZIELONEJ
			 * ZAPIS DO LISTY ZE ZLECENIAMI I UTWORZENIE WATKOW STANOWISK */

			/* Ustawienie handlerow dla sygnalow (wysylanych przez proces managera) */
			signal(SIGINT, signalWhenManagerSendOrder);
			signal(SIGQUIT, signalTheManagerMustEnd);

			initTotalData(); /* inicjalizacja struktury z przychodami warsztatu */
			generateWorkstations(); /* GENERATOR STANOWISK (WATKOW) */

			service_shm_data = (int *) shmat(shmid, 0, 0); /* pamiec dzielona */
			serviceWork(service_shm_data);
			shmdt(service_shm_data);

			joinAndTerminateWorkstationsThreads();
			printf("\nWarsztat zakończył pracę.\n");

		} else {
			/* MANAGER PROCESS - MANAGER WARSZTATU, ODCZYT ZLECEN PRZY POMOCY POTOKU
			 * PRZEKAZANIE ZLECEN DO WARSZTATU PRZY POMOCY PAMIECI DZIELONEJ */
			manager_shm_data = (int *) shmat(shmid, 0, 0);

			close(pipe_fd[WRITE]); /* Zamkniecie wyjsciowej strony potoku (pipe) */
			while (workOn) {
				/* Odczyt wiadomosci przeslanej potoku (pipe) */
				read(pipe_fd[READ], readbuffer, sizeof(readbuffer));
				char car_id;
				int damage_degree;
				/* Odczyt danych ze stringu */
				sscanf(readbuffer, "Car id: %c, damage degree: %i", &car_id, &damage_degree);
				manager_shm_data[0] = car_id;
				manager_shm_data[1] = damage_degree;
				printf("\nManager otrzymał zlecenie od klienta: car_id: %c, uszkodzenie: %d\n",
						manager_shm_data[0], manager_shm_data[1]);
				kill(pid2, SIGINT); /* Sygnal dla warsztatu ze jest nowe zlecenie */
				sleep((rand()%2)+0.5); /* Czas na odebranie sygnalu przez proces warsztatu */
				checkTheTime(end_time);
			}
			printf("\nManager kończy prace.\n");
			kill(pid2, SIGQUIT);

		    int returnStatus;
		    waitpid(pid2, &returnStatus, 0); /* Proces oczekuje az proces dziecko (warsztat) zakonczy prace */

		    /* detach i czyszczenie pamieci dzielonej */
			shmdt(manager_shm_data);
			shmctl(shmid, IPC_RMID, 0);
		}
	}

	return 0;
}

void initRandomNumberGenerator(void) {
	time_t t;
	srand((unsigned) time(&t));
}

void checkTheTime(time_t end) {
	if (time(NULL) == end) {
		workOn = 0;
	}
}

int forkedFailed(pid_t pid) {
	if (pid < 0) { // fork niepowodzenie
		printf("Nie udało się utworzyć procesu! Zakończenie programu!\n");
		return 1;
	}
	return 0;
}

void signalWhenManagerSendOrder() {
	signal(SIGINT, signalWhenManagerSendOrder); /* reset sygnalu */
	signalFromManager = 1; /* ustawienie zmiennej globalnej */
}

void signalTheManagerMustEnd() {
	signal(SIGQUIT, signalTheManagerMustEnd); /* reset sygnalu */
	breakFlag = 1; /* ustawienie zmiennej globalnej */
}

void serviceWork(int * shared_data) {
	while (1) {
		if (breakFlag) {
			break;
		}
		if (signalFromManager) {
			char car_id = shared_data[0];
			int damage_degree = shared_data[1];
			float cost = rand() % 5000;
			addJobToList(car_id, cost, damage_degree);
			printf("\nWarsztat otrzymał zlecenie od managera: car_id: %c, uszkodzenie: %d, "
					"wycena mechanika: %2.2f zł.\n", car_id, damage_degree, cost);
			signalFromManager = 0;
		}
	}
}
