#include "header.h"

void addJobToList(int car_id, double repair_cost, int damage_degree) {
	item = malloc(sizeof(*item));
	if (item == NULL) {
		perror("malloc failed");
		exit(EXIT_FAILURE);
	}

	/* Ustaw wartosci dla zlecenia */
	item->car_id = car_id;
	item->repair_cost = repair_cost;
	item->damage_degree = damage_degree;

	TAILQ_INSERT_TAIL(&order_data_head, item, entries);
}

void initTotalData(void) {
	total.total_sum = 0.0;
	total.num_of_repaired_cars = 0;
}
